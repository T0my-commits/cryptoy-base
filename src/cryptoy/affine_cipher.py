from math import (
    gcd,
)

from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)

# TP: Chiffrement affine


def compute_permutation(a: int, b: int, n: int) -> list[int]:
    # A implémenter, en sortie on doit avoir une liste result tel que result[i] == (a * i + b) % n
    if gcd(a, n) != 1:
        raise RuntimeError(f"a={a} and N={n} are not prime between them")
    result = [(a * i + b) % n for i in range(n)]
    return result


def compute_inverse_permutation(a: int, b: int, n: int) -> list[int]:
    # A implémenter, pour cela on appelle perm = compute_permutation(a, b, n) et on calcule la permutation inverse
    # result qui est telle que: perm[i] == j implique result[j] == i
    if gcd(a, n) != 1:
        raise RuntimeError(f"a={a} and N={n} are not prime between them")
    perm = compute_permutation(a, b, n)
    inverse_perm = [None] * n
    for i, val in enumerate(perm):
        inverse_perm[val] = i
    return inverse_perm


def encrypt(msg: str, a: int, b: int) -> str:
    # A implémenter, en utilisant compute_permutation, str_to_unicodes et unicodes_to_str
    permutations = compute_permutation(a, b, 0x110000)
    encrypted_msg = list()
    for c in str_to_unicodes(msg):
        encrypted_msg.append(permutations[c])
    return unicodes_to_str(encrypted_msg)


def encrypt_optimized(msg: str, a: int, b: int) -> str:
    # A implémenter, sans utiliser compute_permutation
    return unicodes_to_str(
       [(a * c + b) % 0x110000 for c in str_to_unicodes(msg)]
    )


def decrypt(msg: str, a: int, b: int) -> str:
    # A implémenter, en utilisant compute_inverse_permutation, str_to_unicodes et unicodes_to_str
    permutations = compute_inverse_permutation(a, b, 0x110000)
    decrypted_msg = list()
    for c in str_to_unicodes(msg):
        decrypted_msg.append(permutations[c])
    return unicodes_to_str(decrypted_msg)


def decrypt_optimized(msg: str, a_inverse: int, b: int) -> str:
    # A implémenter, sans utiliser compute_inverse_permutation
    # On suppose que a_inverse a été précalculé en utilisant compute_affine_key_inverse, et passé
    # a la fonction
    return unicodes_to_str(
        [(a_inverse * (c - b)) % 0x110000 for c in str_to_unicodes(msg)]
    )


def compute_affine_keys(n: int) -> list[int]:
    # A implémenter, doit calculer l'ensemble des nombre a entre 1 et n tel que gcd(a, n) == 1
    # c'est à dire les nombres premiers avec n
    keys = [a for a in range(1, n) if gcd(a, n) == 1]
    return keys


def compute_affine_key_inverse(a: int, affine_keys: list, n: int) -> int:
    # Trouver a_1 dans affine_keys tel que a * a_1 % N == 1 et le renvoyer
    # Placer le code ici (une boucle)
    for a_1 in affine_keys:
        if (a * a_1) % n == 1:
            return a_1

    # Si a_1 n'existe pas, alors a n'a pas d'inverse, on lance une erreur:
    raise RuntimeError(f"{a} has no inverse")


def attack() -> tuple[str, tuple[int, int]]:
    s = "࠾ੵΚઐ௯ஹઐૡΚૡೢఊஞ௯\u0c5bૡీੵΚ៚Κஞїᣍફ௯ஞૡΚր\u05ecՊՊΚஞૡΚՊեԯՊ؇ԯրՊրր"
    # trouver msg, a et b tel que affine_cipher_encrypt(msg, a, b) == s
    # avec comme info: "bombe" in msg et b == 58
    b = 58
    afks = compute_affine_keys(0x110000)
    for afk in afks:
        se = decrypt(s, afk, b)
        print(f"a={afk} b={b}\t{se}")
        if "bombe" in se:
            return (se, (afk, b)) # ("La bombe explosera à l'école 2600 le 01/07/2022", (27, 58))

    raise RuntimeError("Failed to attack")


def attack_optimized() -> tuple[str, tuple[int, int]]:
    s = (
        "જഏ൮ൈ\u0c51ܲ೩\u0c51൛൛అ౷\u0c51ܲഢൈᘝఫᘝా\u0c51\u0cfc൮ܲఅܲᘝ൮ᘝܲాᘝఫಊಝ"
        "\u0c64\u0c64ൈᘝࠖܲೖఅܲఘഏ೩ఘ\u0c51ܲ\u0c51൛൮ܲఅ\u0cfc\u0cfcඁೖᘝ\u0c51"
    )
    # trouver msg, a et b tel que affine_cipher_encrypt(msg, a, b) == s
    # avec comme info: "bombe" in msg
    afks = compute_affine_keys(0x110000)
    for afk in afks:
        for b in range(1, 10000):
            se = decrypt_optimized(s, compute_affine_key_inverse(afk, afks, 0x110000), b)
            print(f"a={afk} b={b}\t{se}")
            if "bombe" in se:
                return (se, (afk, b)) # ('Notre message précédent a été déchiffré, la bombe est annulée', (19, 1234))

    raise RuntimeError("Failed to attack")
